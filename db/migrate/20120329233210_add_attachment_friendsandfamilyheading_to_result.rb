class AddAttachmentFriendsandfamilyheadingToResult < ActiveRecord::Migration
  def self.up
    add_column :results, :friendsandfamilyheading_file_name, :string
    add_column :results, :friendsandfamilyheading_content_type, :string
    add_column :results, :friendsandfamilyheading_file_size, :integer
    add_column :results, :friendsandfamilyheading_updated_at, :datetime
  end

  def self.down
    remove_column :results, :friendsandfamilyheading_file_name
    remove_column :results, :friendsandfamilyheading_content_type
    remove_column :results, :friendsandfamilyheading_file_size
    remove_column :results, :friendsandfamilyheading_updated_at
  end
end

class AddProductLinkToResults < ActiveRecord::Migration
  def up
    add_column :results, :product_link, :string
  end

  def down
    remove_column :results, :product_link
  end
end

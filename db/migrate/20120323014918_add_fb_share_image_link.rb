class AddFbShareImageLink < ActiveRecord::Migration
  def up
    add_column :results, :fb_share_image_link, :string
  end

  def down
    remove_column :results, :fb_share_image_link
  end
end

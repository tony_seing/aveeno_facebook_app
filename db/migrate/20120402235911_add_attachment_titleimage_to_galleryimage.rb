class AddAttachmentTitleimageToGalleryimage < ActiveRecord::Migration
  def self.up
    add_column :galleryimages, :titleimage_file_name, :string
    add_column :galleryimages, :titleimage_content_type, :string
    add_column :galleryimages, :titleimage_file_size, :integer
    add_column :galleryimages, :titleimage_updated_at, :datetime
  end

  def self.down
    remove_column :galleryimages, :titleimage_file_name
    remove_column :galleryimages, :titleimage_content_type
    remove_column :galleryimages, :titleimage_file_size
    remove_column :galleryimages, :titleimage_updated_at
  end
end

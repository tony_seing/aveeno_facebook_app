class AddBackgroundColumnToResult < ActiveRecord::Migration
  def self.up
    change_table :results do |t|
      t.has_attached_file :background
    end
  end

  def self.down
    drop_attached_file :results, :background
  end
end

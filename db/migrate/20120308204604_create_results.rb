class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.string :name
      t.text :what_that_means
      t.text :friends_and_family
      t.text :in_nature
      t.text :beautiful_change_idea
      t.string :product_title
      t.text :product_description

      t.timestamps
    end
  end
end

class AddAttachmentWhatthatmeansheadingToResult < ActiveRecord::Migration
  def self.up
    add_column :results, :whatthatmeansheading_file_name, :string
    add_column :results, :whatthatmeansheading_content_type, :string
    add_column :results, :whatthatmeansheading_file_size, :integer
    add_column :results, :whatthatmeansheading_updated_at, :datetime
  end

  def self.down
    remove_column :results, :whatthatmeansheading_file_name
    remove_column :results, :whatthatmeansheading_content_type
    remove_column :results, :whatthatmeansheading_file_size
    remove_column :results, :whatthatmeansheading_updated_at
  end
end

class Authentication < ActiveRecord::Base
   validates :name, :presence => true
   validates :what_it_means, :presence => true
   validates :friends_and_family, :presence => true
   validates :in_nature, :presence => true
   validates :beautiful_change_idea, :presence => true
   validates :product_title, :presence => true
   validates :product_description, :presence => true
   
end

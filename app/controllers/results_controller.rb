class ResultsController < ApplicationController
  before_filter :authenticate_administrator!, :except => ['display', 'landing_page']
 
  layout 'results'
  
  
  # GET /results
  # GET /results.json
  def index
    @results = Result.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @results }
    end
  end

  # GET /results/1
  # GET /results/1.json
  def show
    @result = Result.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @result }
    end
  end
  
  
  def landing_page
    @images = Galleryimage.find(:all)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @result }
    end
  end
  
  def reveal_coupon_action
      if params.has_key?(:post_id)
	cookies[:revealed_coupon] = 'true'  
	session[:revealed_coupon] = 'true'
      end
      redirect_to '/auth/facebook'
  end

  
  
  
  # GET /results/1
  # GET /results/1.json
  def display
      #  response = Net::HTTP.get_response("example.com","/?search=thing&format=json")
     if params.has_key?(:testmonth) # for testing
       @birthday = params[:testmonth].to_i
     else # naturally occuring case
       @birthday = Date.strptime(request.env["omniauth.auth"]["extra"]["raw_info"]["birthday"], "%m/%d/%Y").month if request.env["omniauth.auth"]["extra"]["raw_info"]["birthday"].present?
     end
   
      @output = case
      when @birthday == 1
	"Calming Feverfew"
      when @birthday == 2
	"Protective Lotus"
      when @birthday == 3
	"Balancing Seaweed Extract"
      when @birthday == 4
	"Revitalizing Shiitake"
      when @birthday == 5
	"Fortifying Southernwood"
      when @birthday == 6
	"Revitalizing Shiitake"
      when @birthday == 7
	"Protective Lotus"
      when @birthday == 8
	"Vibrant Lupine"
      when @birthday == 9
	"Nurturing Oat"
      when @birthday == 10
	"Resilient Soy"
      when @birthday == 11
	"Balancing Seaweed Extract"
      when @birthday == 12
	"Focused Wheat"
      else
	"Nurturing Oat"
      end
    @result = Result.find_by_name(@output)
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @result }
    end
  end

  
  
  # GET /results/new
  # GET /results/new.json
  def new
    @result = Result.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @result }
    end
  end

  # GET /results/1/edit
  def edit
    @result = Result.find(params[:id])
  end

  # POST /results
  # POST /results.json
  def create
    @result = Result.new(params[:result])

    respond_to do |format|
      if @result.save
        format.html { redirect_to @result, notice: 'Result was successfully created.' }
        format.json { render json: @result, status: :created, location: @result }
      else
        format.html { render action: "new" }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /results/1
  # PUT /results/1.json
  def update
    @result = Result.find(params[:id])

    respond_to do |format|
      if @result.update_attributes(params[:result])
        format.html { redirect_to @result, notice: 'Result was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /results/1
  # DELETE /results/1.json
  def destroy
    @result = Result.find(params[:id])
    @result.destroy

    respond_to do |format|
      format.html { redirect_to results_url }
      format.json { head :ok }
    end
  end
end

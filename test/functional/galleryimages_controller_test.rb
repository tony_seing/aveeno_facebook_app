require 'test_helper'

class GalleryimagesControllerTest < ActionController::TestCase
  setup do
    @galleryimage = galleryimages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:galleryimages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create galleryimage" do
    assert_difference('Galleryimage.count') do
      post :create, galleryimage: @galleryimage.attributes
    end

    assert_redirected_to galleryimage_path(assigns(:galleryimage))
  end

  test "should show galleryimage" do
    get :show, id: @galleryimage.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @galleryimage.to_param
    assert_response :success
  end

  test "should update galleryimage" do
    put :update, id: @galleryimage.to_param, galleryimage: @galleryimage.attributes
    assert_redirected_to galleryimage_path(assigns(:galleryimage))
  end

  test "should destroy galleryimage" do
    assert_difference('Galleryimage.count', -1) do
      delete :destroy, id: @galleryimage.to_param
    end

    assert_redirected_to galleryimages_path
  end
end
